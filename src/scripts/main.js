$(document).ready(function() {
    $(window).resize(function(){
        $('.content section').css('height', '100%');
  });
    $('.language').click(function(){
        var block = $('.content section').height();
        $('.content section').css('height', block);
        if($('.language__ru').css('display') === 'inline'){
            $('.en').slideUp();
                $('.ru').slideDown(1000);
                    $('.language__ru').css('display', 'none');
                        $('.language__en').fadeIn(2000);
        }
        else{
            $('.ru').slideUp();
                $('.en').slideDown(1000);
                    $('.language__en').css('display', 'none');
                        $('.language__ru').fadeIn(2000);
        }
    });
});
